package com.dm.labs.vaadin.frontend;

import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.Route;

@Route("app")
public class MainAppLayout extends AppLayout {
    public MainAppLayout() {
        setPrimarySection(AppLayout.Section.DRAWER);
        Image img = new Image("https://i.imgur" +
                ".com/GPpnszs.png", "Vaadin Logo");
        img.setHeight("44px");
        addToNavbar(new DrawerToggle(), img);
        Tabs tabs = new Tabs(new Tab("Home"),
                new Tab("About"));
        Tab home = new Tab("Home");
        tabs.addSelectedChangeListener(event -> event.getSelectedTab());
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        addToDrawer(tabs);
    }
}
