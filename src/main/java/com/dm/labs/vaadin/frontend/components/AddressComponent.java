package com.dm.labs.vaadin.frontend.components;

import com.dm.labs.vaadin.backend.Address;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

public class AddressComponent extends CustomField<Address> {

    private TextField street = new TextField();
    private TextField city = new TextField();
    private TextField zip = new TextField();
    private TextField country = new TextField();

    public AddressComponent() {
        street.setLabel("Street");
        street.setPlaceholder("10 Downing Street");
        city.setLabel("City");
        city.setPlaceholder("London");
        zip.setLabel("Zip");
        zip.setPlaceholder("SW1A 2AA");
        country.setLabel("Country");
        country.setPlaceholder("United Kingdom");
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(street, city, zip, country);
        add(verticalLayout);
    }

    @Override
    protected Address generateModelValue() {
        Address address = new Address();
        address.setCity(city.getValue());
        address.setStreet(street.getValue());
        address.setZip(zip.getValue());
        address.setCountry(country.getValue());
        return address;
    }

    @Override
    protected void setPresentationValue(Address address) {
        street.setValue(address.getStreet());
        city.setValue(address.getCity());
        zip.setValue(address.getZip());
        country.setValue(address.getCountry());
    }
}
