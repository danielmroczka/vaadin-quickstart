package com.dm.labs.vaadin.frontend;

import com.dm.labs.vaadin.backend.Customer;
import com.dm.labs.vaadin.backend.CustomerRepository;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

@Route("customer")
public class MainCustomerView extends VerticalLayout {
    @Autowired
    CustomerRepository customerRepository;
    private CustomerForm form = new CustomerForm(this);
    private Grid<Customer> grid = new Grid<>(Customer.class);
    private TextField filterText = new TextField();


    public MainCustomerView() {
        HorizontalLayout mainContent = new HorizontalLayout(grid, form);
        mainContent.setSizeFull();
        grid.setSizeFull();

        add(filterText, mainContent);
    }

    @PostConstruct
    private void init() {

    }

    public void updateList() {
//        List<Customer> customers = customerRepository.findAll(filterText.getValue());
//        grid.setItems( customers );
    }
}
