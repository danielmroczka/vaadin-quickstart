package com.dm.labs.vaadin.frontend;

import com.dm.labs.vaadin.backend.Item;
import com.dm.labs.vaadin.backend.ItemService;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route("crud")
public class CrudView extends VerticalLayout {

    TextField firstName = new TextField("First Name");
    TextField lastName = new TextField("Last Name");
    Item item = new Item();
    private LogDisplay logDisplay = new LogDisplay();


    @Autowired
    private ItemService itemService;

    public CrudView() {
        Binder<Item> binder = new BeanValidationBinder<>(Item.class);
        binder.bindInstanceFields(this);
        binder.setBean(item);

        FormLayout layoutWithFormItems = new FormLayout();


        firstName.setPlaceholder("John");
        // layoutWithFormItems.addFormItem(firstName, "First name");


        lastName.setPlaceholder("Doe");
        //  layoutWithFormItems.addFormItem(lastName, "Last name");

        Button button = new Button("Submit", event -> {
            if (binder.validate().isOk()) {
                save(firstName.getValue(), lastName.getValue());
                logDisplay.log("Success!");
                item = new Item();
                binder.setBean(item);
                UI.getCurrent().navigate("");
            }

        });

        button.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        add(firstName, lastName, button, logDisplay);
        // layoutWithFormItems.add(button);

        add(layoutWithFormItems);
    }

    private void save(String firstName, String lastName) {

        item.setFirstName(firstName);
        itemService.add(item);
    }

    public static class LogDisplay extends Composite<VerticalLayout> {

        public void log(String message) {
            if (getContent().getComponentCount() > 4) {
                getContent().removeAll();
            }
            getContent().add(new Paragraph(message));
        }
    }
}
