package com.dm.labs.vaadin.frontend;

import com.dm.labs.vaadin.frontend.components.AddressComponent;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.contextmenu.ContextMenu;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.timepicker.TimePicker;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;

import java.time.Duration;

@Route("sandbox")
@CssImport("./styles/style.css")
public class SandboxView extends VerticalLayout {
    public SandboxView() {
        init();
    }

    private void init() {
        Accordion accordion = new Accordion();
        TextField textField = new TextField();
        textField.setLabel("Text field label");
        textField.setPlaceholder("placeholder text");
        textField.setClearButtonVisible(true);
        textField.setValueChangeMode(ValueChangeMode.TIMEOUT);
        textField.setValueChangeTimeout(1000);
        textField.addValueChangeListener(event -> System.out.println(event.getValue()));

        NumberField dollarField = new NumberField("Dollars");
        dollarField.setPrefixComponent(new Span("$"));
        NumberField euroField = new NumberField("Euros");
        euroField.setSuffixComponent(new Span("€"));
        NumberField stepperField = new NumberField("Stepper");
        stepperField.setValue(1d);
        stepperField.setMin(0);
        stepperField.setMax(10);
        stepperField.setHasControls(true);

        Checkbox checkbox = new Checkbox();
        checkbox.setLabel("My Label");
        checkbox.setIndeterminate(true);

        Select<String> select = new Select("Option one", "Option two");
        select.setPlaceholder("Placeholder");
        select.setLabel("Label");


        ListBox<String> listBox = new ListBox<>();
        listBox.setItems("Bread", "Butter", "Milk");

        TimePicker timePicker = new TimePicker();
        timePicker.setClearButtonVisible(true);
        timePicker.setPlaceholder("Placeholder");
        timePicker.setStep(Duration.ofMinutes(15));
        timePicker.setMin("08:00");
        timePicker.setMax("17:00");

        AddressComponent addressComponent = new AddressComponent();

        ContextMenu contextMenu = new ContextMenu();

        contextMenu.setTarget(this);
        Label message = new Label("-");
        contextMenu.addItem("First menu item",
                e -> message.setText("Clicked on " +
                        "the first item"));
        contextMenu.addItem("Second menu item",
                e -> message.setText("Clicked on " +
                        "the second item"));
        MenuItem item = contextMenu.addItem("Disabled " +
                        "menu item",
                e -> message.setText("This cannot happen"));
        item.setEnabled(false);

        Details details = new Details("Expandable Details",
                new Text("Toggle using mouse, Enter " +
                        "and Space keys."));

        Notification notification = new Notification(
                "This notification has text content", 3000);
        // notification.setPosition(Po);
        Button button = new Button("Notify");
        button.addClickListener(event -> notification.open());


        add(textField, dollarField, checkbox, listBox, select, timePicker, addressComponent, message, details, button);


    }
}
