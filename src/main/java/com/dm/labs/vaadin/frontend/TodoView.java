package com.dm.labs.vaadin.frontend;

import com.dm.labs.vaadin.backend.Todo;
import com.dm.labs.vaadin.backend.TodoRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route("todo")
public class TodoView extends VerticalLayout {

    @Autowired
    TodoRepository todoRepository;

    VerticalLayout list = new VerticalLayout();
    Button add = new Button("Add");
    Todo todo = new Todo();

    public TodoView() {

        final FormLayout formLayout = add();
        list.add(formLayout);
        add.addClickListener(event -> {
            todoRepository.save(todo);
            todo = new Todo();
            FormLayout item = add();
            //addItem(list, todo);
            list.add(item);


        });


        add(list, add);

    }

    FormLayout add() {
        FormLayout formLayout = new FormLayout();
        formLayout.setWidth("300px");

        TextField title = new TextField();
        DatePicker deadline = new DatePicker();
        Checkbox status = new Checkbox();
        status.addValueChangeListener(event -> {
            title.setEnabled(!event.getValue());
            deadline.setEnabled(!event.getValue());
        });
        formLayout.add(title, deadline, status);

        Binder<Todo> binder = new Binder(Todo.class);
        // binder.bindInstanceFields();

        binder.bind(title, "title");
        binder.bind(deadline, "deadline");
        binder.bind(status, "status");
        binder.setBean(todo);
        return formLayout;
    }

//    private void addItem(VerticalLayout list, FormLAyout for Todo todo) {
//        list.add(new Button("ADD"));
//    }


}
