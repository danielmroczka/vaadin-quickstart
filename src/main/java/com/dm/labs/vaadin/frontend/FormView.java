package com.dm.labs.vaadin.frontend;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.Route;

@Route("form")
public class FormView extends VerticalLayout implements BeforeLeaveObserver {
    public FormView() {
        FormLayout nameLayout = new FormLayout();
        TextField titleField = new TextField();
        titleField.setLabel("Title");
        titleField.setPlaceholder("Sir");
        TextField firstNameField = new TextField();
        firstNameField.setLabel("First name");
        firstNameField.setPlaceholder("John");
        TextField lastNameField = new TextField();
        lastNameField.setLabel("Last name");
        lastNameField.setPlaceholder("Doe");
        nameLayout.add(titleField, firstNameField,
                lastNameField);
        nameLayout.setResponsiveSteps(
                new FormLayout.ResponsiveStep("0", 1),
                new FormLayout.ResponsiveStep("21em", 2),
                new FormLayout.ResponsiveStep("22em", 3));

        add(nameLayout);


        Page page = UI.getCurrent().getPage();
        page.addBrowserWindowResizeListener(
                event -> Notification.show("Window width="
                        + event.getWidth()
                        + ", height=" + event.getHeight()));
    }

    @Override
    public void beforeLeave(BeforeLeaveEvent event) {

        BeforeLeaveEvent.ContinueNavigationAction action =
                event.postpone();

        action.proceed();

    }
}
