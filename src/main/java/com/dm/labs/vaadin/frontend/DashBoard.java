package com.dm.labs.vaadin.frontend;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.material.Material;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Route("")
@PWA(name = "Vaadin Quickstart", shortName = ".")
@Theme(value = Material.class, variant = Material.DARK)
@CssImport("./styles/style.css")
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
public class DashBoard extends VerticalLayout implements RouterLayout {
    private TextField textField = new TextField();
    private EmailField emailField = new EmailField();
    private PasswordField passwordField = new PasswordField();
    private ListBox<String> listBox = new ListBox();
    private ProgressBar progressBar = new ProgressBar();

    private Tabs tabs;

    public DashBoard() {
        textField.setLabel("Input Text");
        textField.setPlaceholder("Placeholder");
        textField.addKeyUpListener(event -> System.out.println("Key Up" + event.getCode()));

        passwordField.setLabel("Provide password");

        listBox.setItems("Audi", "BMW", "Citroen");

        Tab tab1 = new Tab("Tab one");
        Tab tab2 = new Tab("Tab two");
        Tab tab3 = new Tab("Tab three");

        tabs = new Tabs(tab1, tab2, tab3);
        Div div = new Div();
        tabs.addSelectedChangeListener(event -> div.setText(event.getSelectedTab().getLabel()));

        Icon edit = new Icon(VaadinIcon.EDIT);
        Icon close = VaadinIcon.CLOSE.create();

        getStyle().set("border", "1px solid #9E9E9E");
        add(textField, emailField, passwordField, listBox, passwordField, tabs, div, edit, close);
    }

    @PostConstruct
    public void progress() throws InterruptedException {
        for (int i = 0; i < 100; i++) {
            progressBar.setValue((double) i / 100);
            TimeUnit.MILLISECONDS.sleep(100);
        }
    }
}
