package com.dm.labs.vaadin.frontend;

import com.dm.labs.vaadin.backend.Item;
import com.dm.labs.vaadin.backend.ItemRepository;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

@Route("main")
public class MainView extends VerticalLayout {
    private TextField filterText = new TextField();
    private Grid grid = new Grid<>(Item.class);

    @Autowired
    private ItemRepository repo;

    public MainView() {

        grid.setColumns("firstName", "lastName");
        add(filterText, grid);
        add(new Button("Load All", e -> loadAll()));

        filterText.setClearButtonVisible(true);
        filterText.setPlaceholder("Filter by");
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());
    }

    @PostConstruct
    public void loadAll() {

        List<Item> list = repo.findAll();
        grid.setItems(list);
        //  grid.setColumns("first name", "last name");
        //  grid.addColumn(Item::getLastName);
        Notification.show("Reloaded " + list.size() + " items", 1000, Notification.Position.BOTTOM_END);
    }

    public void updateList() {
        grid.setItems(repo.findByFirstNameIgnoreCaseContainingOrLastNameIgnoreCaseContaining(filterText.getValue(), filterText.getValue()));
    }
}
