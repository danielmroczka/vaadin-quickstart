package com.dm.labs.vaadin.frontend;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;

@Route(value = "demo", layout = DashBoard.class)
public class DemoView extends VerticalLayout {
    private TextField textField = new TextField();

    public DemoView() {
        textField.setLabel("Input Text");
        textField.addKeyUpListener(event -> System.out.println("Key Up" + event.getCode()));
        add(textField);
    }

}
