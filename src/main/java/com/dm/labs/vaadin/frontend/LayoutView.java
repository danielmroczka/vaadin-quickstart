package com.dm.labs.vaadin.frontend;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("layout")
public class LayoutView extends VerticalLayout {
    public LayoutView() {
        init();
    }

    private void init() {
        HorizontalLayout hl1 = new HorizontalLayout();

        Label lb1 = new Label("label1");
        Label lb2 = new Label("label2");


        hl1.add(lb1, lb2);

        add(hl1);

    }
}
