package com.dm.labs.vaadin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaadinQuickstartApplication {

    public static void main(String[] args) {
        SpringApplication.run(VaadinQuickstartApplication.class, args);
    }

}
