package com.dm.labs.vaadin.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/item")
public class ItemController {

    @Autowired
    public ItemRepository itemRepository;

    @PostMapping
    public void add(@RequestBody Item item) {
        itemRepository.save(item);
    }
}
