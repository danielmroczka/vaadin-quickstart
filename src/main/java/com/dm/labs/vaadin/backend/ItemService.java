package com.dm.labs.vaadin.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ItemService {
    @Autowired
    private ItemRepository repository;

    public Item add(Item item) {
        return repository.save(item);
    }


}
